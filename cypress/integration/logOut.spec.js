/* eslint-disable no-undef */
describe("Log in and then log out", () => {
	it("logs in from the start page", () => {
		cy.visit("/");
		cy.get(".login-button-header").click();

		cy.url().should("eq", "http://localhost:3000/prosjekt3/login");

		// log in so that we can test signing out
		cy.get(".login-username").type("Tester");
		cy.get(".login-password").type("TestingYeh");
		cy.get(".login-button").click();
	});

	it("signs out from the start page", () => {
		cy.get(".signout-button-header")
			.click()
			.should(() => expect(localStorage.getItem("token")).to.be.null);
	});
});
