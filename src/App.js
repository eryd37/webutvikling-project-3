import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
	TopAppBar,
	TopAppBarRow,
	TopAppBarSection,
	TopAppBarTitle,
} from "@rmwc/top-app-bar";
import { Typography } from "@rmwc/typography";
import { Icon } from "@rmwc/icon";
import { LinearProgress } from "@rmwc/linear-progress";
import { Button } from "@rmwc/button";

import "./App.scss";
import Search from "./components/Search/Search";
import Recipe from "./components/Recipe/Recipe";
import CreateRecipe from "./components/CreateRecipe/CreateRecipe";
import Wordcloud from "./components/Wordcloud/Wordcloud";
import Tabs from "./components/Tabs/Tabs";
import SearchHistory from "./components/SearchHistory/SearchHistory";
import Signup from "./components/Signup/Signup";
import Login from "./components/Login/Login";
import { setUser, signOut } from "./redux/actions";
import { authenticated } from "./server/queries";

const App = props => {
	useEffect(() => {
		if (localStorage.getItem("token")) {
			authenticated()
				.then(res => {
					if (res.data) {
						props.dispatch(
							setUser(
								localStorage.getItem("userID"),
								localStorage.getItem("token"),
								localStorage.getItem("username")
							)
						);
					} else {
						console.error(res.error);
						handleSignOut();
					}
				})
				.catch(error => console.error(error));
		}
	}, []);

	function handleSignOut() {
		props.dispatch(signOut());
		localStorage.removeItem("token");
		localStorage.removeItem("userID");
		localStorage.removeItem("username");
	}

	return (
		<div className='main-container'>
			<Router>
				<LinearProgress closed={!props.loading} className='progress-bar' />
				<TopAppBar>
					<TopAppBarRow>
						<TopAppBarSection>
							<TopAppBarTitle>
								<Link to='/prosjekt3/'>
									<Icon
										className='title-logo'
										icon={{
											icon: process.env.PUBLIC_URL + "/goose.png",
											size: "medium",
										}}
									/>
									<Typography use='headline5' className='headline'>
										GooseGooseRun - Recipes
									</Typography>
								</Link>
							</TopAppBarTitle>
						</TopAppBarSection>
						<TopAppBarSection alignEnd>
							{props.username ? (
								<div className='login-info'>
									<div className='username'>
										<Icon icon='account_circle' />
										<span className='username-text'>{props.username}</span>
									</div>
									<div>
										<Button
											className='header-button signout-button-header'
											onClick={() => handleSignOut()}
										>
											Sign out
										</Button>
									</div>
								</div>
							) : (
								<div>
									<Link to='/prosjekt3/signup'>
										<Button className='header-button signup-button-header' type='button'>
											Sign up
										</Button>
									</Link>
									<Link to='/prosjekt3/login'>
										<Button className='header-button login-button-header' type='button'>
											Log in
										</Button>
									</Link>
								</div>
							)}
						</TopAppBarSection>
					</TopAppBarRow>
				</TopAppBar>
				<div className='content-wrapper'>
					<Tabs />
					<main>
						<div className='content'>
							<Switch>
								<Route path='/prosjekt3/login'>
									<Login />
								</Route>
								<Route path='/prosjekt3/signup'>
									<Signup />
								</Route>
								<Route path='/prosjekt3/recipes/create'>
									<CreateRecipe />
								</Route>
								<Route path='/prosjekt3/recipes/:id'>
									<Recipe />
								</Route>
								<Route path='/prosjekt3/wordcloud'>
									<Wordcloud />
								</Route>
								<Route path='/prosjekt3/history'>
									<SearchHistory />
								</Route>
								<Route path='/prosjekt3/'>
									<Search />
								</Route>
								<Route path="/">
									<Redirect to="/prosjekt3/" />
								</Route>
							</Switch>
						</div>
					</main>
				</div>
			</Router>
		</div>
	);
};

const mapStateToProps = state => ({
	loading: state.loading.loading,
	username: state.user.username,
});

export default connect(mapStateToProps)(App);
