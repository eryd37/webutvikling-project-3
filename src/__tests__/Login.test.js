import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import { MemoryRouter} from "react-router-dom"
import { render, getByText, getByLabelText } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Login from "../components/Login/Login";
import { Provider } from "react-redux";
import store from "../redux/store";

let container = null;
beforeEach(() => {
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders without crashing", () => {
	ReactDOM.render(
		<Provider store={store}>
			<MemoryRouter>
        <Login />
      </MemoryRouter>
		</Provider>,
		container
	);
});

it("renders if given token", () => {
	ReactDOM.render(
		<Provider store={store}>
			<MemoryRouter>
        <Login token={"yes"} />
      </MemoryRouter>
		</Provider>,
		container
	);
});
