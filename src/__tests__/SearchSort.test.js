import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import { render, getByText, getByLabelText } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import SearchResult from "../components/Search/SearchResult";
import SearchSort from "../components/Search/SearchSort";
import { Provider } from "react-redux";
import store from "../redux/store";

let container = null;
beforeEach(() => {
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders without crashing", () => {
	ReactDOM.render(
		<Provider store={store}>
			<SearchSort />
		</Provider>,
		container
	);
});
