import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import { render, getByText, getByLabelText } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Searchfield from "../components/Search/Searchfield";
import { Provider } from "react-redux";
import store from "../redux/store";

let container = null;
beforeEach(() => {
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders without crashing", () => {
	ReactDOM.render(
		<Provider store={store}>
			<Searchfield />
		</Provider>,
		container
	);
});

it("contains the label text", () => {
	act(() => {
		ReactDOM.render(
			<Provider store={store}>
				<Searchfield />
			</Provider>,
			container
		);
	});
  expect(getByLabelText(container, "Search for recipes...")).toBeDefined();
});
