import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import { render, getByText, getByLabelText } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Tabs from '../components/Tabs/Tabs';
import { Provider } from 'react-redux';
import store from "../redux/store";
import { MemoryRouter } from 'react-router-dom';

let container = null;
beforeEach(() => {
  Object.defineProperty(global.window, 'scrollTo', { value: jest.fn() });
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders without crashing", () => {
	ReactDOM.render(<Provider store={store}>
    <MemoryRouter>
      <Tabs />
    </MemoryRouter>
  </Provider>, container);
});