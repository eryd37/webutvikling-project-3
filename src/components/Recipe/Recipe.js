import React, { useEffect, useState } from "react";
import { useParams, Link, useHistory } from "react-router-dom";
import { Typography } from "@rmwc/typography";
import { IconButton } from "@rmwc/icon-button";

import "./Recipe.scss";
import { capitalizeFirstLetter } from "./formatUtils";
import { formatText } from "./formatUtils";
import { formatMinutes } from "./formatUtils";
import { getRecipe, deleteRecipe } from "../../server/queries";
import { setLoading } from "../../redux/actions";
import { connect } from "react-redux";
import { Tooltip } from "@rmwc/tooltip";

const Recipe = props => {
	const [recipe, setRecipe] = useState();

	let { id } = useParams();
	const history = useHistory();

	useEffect(() => {
		props.dispatch(setLoading(true));
		getRecipe(id)
			.then(res => {
				setRecipe(res.data.recipe);
			})
			.catch(error => console.error(error))
			.finally(() => props.dispatch(setLoading(false)));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [id]);

	function handleDelete() {
		deleteRecipe(recipe._id)
			.then(res => {
				if (res.error) {
					console.error(res.error);
				} else {
					history.push("/prosjekt3/");
				}
			})
			.catch(error => console.error(error));
	}

	if (!recipe && !props.loading) {
		return <div className='recipe-details'>Error: No recipe found</div>;
	}

	if (!recipe) {
		return <div className='recipe-details'></div>;
	}

	return (
		<div className='recipe-details'>
			<div className='recipe-actions'>
				<Link to='/prosjekt3/'>
					<IconButton icon='arrow_back' className='recipe-back-button' />
				</Link>
				<div className='horizontal-filler'></div>
				<Tooltip content='Delete this recipe'>
					<IconButton
						icon='delete'
						className='recipe-delete-button'
						onClick={() => handleDelete()}
					/>
				</Tooltip>
			</div>
			<div className='recipe-name'>
				<Typography use='headline3' className='headline'>
					{recipe.name}
				</Typography>
			</div>
			<div>
				<Typography use='headline6' className='recipe-time headline'>
					{recipe.minutes
						? formatMinutes(recipe.minutes) + " to prepare"
						: "Prep time not specified"}
				</Typography>
			</div>
			<div className='recipe-description'>
				<Typography use='body1'>{formatText(recipe.description)}</Typography>
			</div>
			<div>
				<Typography use='headline6' className='headline'>
					Ingredients
				</Typography>
			</div>
			<Typography use='body1'>
				<ul className='recipe-ingredients'>
					{recipe.ingredients.map((ingredient, index) => (
						<li className='recipe-ingredient' key={index}>
							{capitalizeFirstLetter(ingredient)}
						</li>
					))}
				</ul>
			</Typography>
			<Typography use='headline6' className='headline'>
				Steps
			</Typography>
			<Typography use='body1'>
				<ol className='recipe-steps'>
					{recipe.steps.map((step, index) => (
						<li className='recipe-step' key={index}>
							{formatText(step)}
						</li>
					))}
				</ol>
			</Typography>
		</div>
	);
};

const mapStateToProps = state => ({
	loading: state.loading.loading,
});

export default connect(mapStateToProps)(Recipe);
