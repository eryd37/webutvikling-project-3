import React, { useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Fab } from "@rmwc/fab";

import Searchfield from "./Searchfield";
import SearchResults from "./SearchResults";
import "./Search.scss";
import SearchSort from "./SearchSort";
import { nextRecipePage } from "../../redux/actions";
import SearchFilters from "./SearchFilters";
import { Tooltip } from "@rmwc/tooltip";

const Search = props => {
	const searchAreaRef = useRef();

	useEffect(() => {
		searchAreaRef.current.addEventListener("scroll", handleScroll);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	function isBottom(el) {
		return el.scrollTop + el.offsetHeight >= el.scrollHeight - 1;
	}

	function handleScroll() {
		const searchEl = searchAreaRef.current;
		if (isBottom(searchEl) && !props.loading) {
			props.dispatch(nextRecipePage());
		}
	}

	return (
		<div className='search-and-filters-wrapper' ref={searchAreaRef}>
			<div className='filters-wrapper'>
				<SearchFilters />
			</div>
			<div className='search-wrapper'>
				<div className='searchfield-wrapper'>
					<Searchfield />
				</div>
				<div className='sort-wrapper'>
					<SearchSort className='sort-field' />
				</div>
				<SearchResults />
			</div>
			<Link to='/prosjekt3/recipes/create'>
				<Tooltip content='Create a recipe'>
					<Fab className='fab-add-recipe' icon='add' type="button" />
				</Tooltip>
			</Link>
		</div>
	);
};

const mapStateToProps = state => ({
	loading: state.loading.loading,
});

export default connect(mapStateToProps)(Search);
