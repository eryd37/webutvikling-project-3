import React, { useState } from "react";
import { List } from "@rmwc/list";
import { connect } from "react-redux";

import SearchResult from "./SearchResult";
import { getSearchCriteria } from "../../redux/selectors";
import "./SearchResults.scss";
import useDeepCompareEffect from "use-deep-compare-effect";
import {
	setLoading,
} from "../../redux/actions";
import getRecipeList from "./searchUtils";
import { addHistory, getSearchHistory } from "../../server/queries";

const SearchResults = props => {
	const [recipes, setRecipes] = useState([]);
	const [error, setError] = useState();

	useDeepCompareEffect(() => {
		if (props.searchCriteria.start === 0) {
			props.dispatch(setLoading(true));
			getRecipeList(props.searchCriteria)
				.then(res => {
					setRecipes(res)
					if (props.token && props.searchCriteria.searchValue.length) {
						getSearchHistory(props.userID)
							.then(res => {
								if (res.data) {
									if (
										!res.data.getSearchHistory.length ||
										props.searchCriteria.searchValue !==
										res.data.getSearchHistory[0].searchValue
									) {
										addHistory(props.searchCriteria.searchValue)
											.then(res => {
												if (res.error) {
													console.error(res.error);
												}
											})
											.catch(error => console.error(error));
									}
								} else {
									console.log(res);
								}
							})
							.catch(error => console.error(error));
					}
				})
				.catch(error => setError(error))
				.finally(() => props.dispatch(setLoading(false)));
		} else {
			props.dispatch(setLoading(true));
			getRecipeList(props.searchCriteria)
				.then(res => {
					const extendedList = recipes.concat(res);
					setRecipes(extendedList);
				})
				.catch(error => setError(error))
				.finally(() => props.dispatch(setLoading(false)));
		}
	}, [props.searchCriteria]);

	if (!recipes.length && !error) {
		return <p>Sorry, there were no results</p>;
	}

	if (error) {
		if (error.message === "recipes.data is undefined") {
			return <p>Sorry, there were no results</p>;
		}
		return <p>Error: {error.message}</p>;
	}

	return (
		<List twoLine className='search-results'>
			{recipes.map((result, index) => (
				<SearchResult result={result} key={index} />
			))}
		</List>
	);
};

const mapStateToProps = state => ({
	searchCriteria: getSearchCriteria(state),
	token: state.user.token,
	userID: state.user.id,
});

export default connect(mapStateToProps)(SearchResults);
