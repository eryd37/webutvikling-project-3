import React from "react";
import { Select } from "@rmwc/select";
import { IconButton } from "@rmwc/icon-button";
import { Tooltip } from "@rmwc/tooltip";
import { connect } from "react-redux";

import "./SearchSort.scss";
import { toggleDescList, setSortField } from "../../redux/actions";

const SearchSort = props => {
	const options = {
		name: "Name",
		minutes: "Prep time",
		n_ingredients: "Number of ingredients",
		n_steps: "Number of steps",
	};

	function handleToggleSort() {
		props.dispatch(toggleDescList());
	}

	function handleChangeSortField(e) {
		props.dispatch(setSortField(e.target.value));
	}

	return (
		<div className='sort-wrapper'>
			<Tooltip content='Toggle sort direction' enterDelay={300}>
				<IconButton
					icon='arrow_upward'
					disabled={!props.searchValue}
					className={"toggle-direction" + (props.sortDESC ? " inverted" : "")}
					onClick={() => {
						handleToggleSort();
					}}
				/>
			</Tooltip>
			<Select
				disabled={!props.searchValue}
				enhanced
				className='sort-field-select'
				label='Sort by'
				value={props.sortField.length ? props.sortField : ""}
				options={options}
				onChange={e => {
					handleChangeSortField(e);
				}}
			/>
		</div>
	);
};

const mapStateToProps = state => ({
	sortDESC: state.recipeSort.sortDESC,
	sortField: state.recipeSort.sortField,
	searchValue: state.recipeSearch.searchValue,
});

export default connect(mapStateToProps)(SearchSort);
