import React from "react";
import { TextField } from "@rmwc/textfield";
import "./Searchfield.scss";
import { connect } from "react-redux";
import { setSearchValue, resetPage } from "../../redux/actions";
import { Tooltip } from "@rmwc/tooltip";

const Searchfield = props => {
	function utilizeSearchValue(searchValue) {
		const stringWithoutBackslash = searchValue.replace(/\\/g, "");
		const stringWithEscapedQuotes = stringWithoutBackslash.replace(/"/g, '\\"');
		props.dispatch(setSearchValue(stringWithEscapedQuotes));
		props.dispatch(resetPage());
	}

	function handleEnterSearch(event) {
		if (event.key === "Enter") {
			utilizeSearchValue(event.target.value);
		}
	}

	function handleSearchClick(e) {
		const searchValue = e.target.nextElementSibling.value;
		utilizeSearchValue(searchValue);
	}

	function handleClearClick(el) {
		el.parentElement.children[1].value = "";
	}

	return (
		<div className='searchfield'>
			<Tooltip
				content='You can use quotes to search for phrases, and - to exclude words'
				enterDelay={1000}
			>
				<TextField
					className='searchfield'
					outlined
					icon={{
						icon: "search",
						tabIndex: 0,
						onClick: e => handleSearchClick(e),
					}}
					trailingIcon={{
						icon: "cancel",
						tabIndex: 0,
						onClick: e => handleClearClick(e.target),
					}}
					defaultValue={props.searchValue}
					helpText={{
						persistent: true,
						children: "Press enter to search",
					}}
					label='Search for recipes...'
					onKeyUp={e => handleEnterSearch(e)}
				/>
			</Tooltip>
		</div>
	);
};
const mapStateToProps = state => ({
	searchValue: state.recipeSearch.searchValue,
});

export default connect(mapStateToProps)(Searchfield);
