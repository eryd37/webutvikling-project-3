import React from "react";
import { ListItem, ListItemGraphic, ListItemMeta } from "@rmwc/list";
import { connect } from "react-redux";
import { setSearchValue, resetPage } from "../../redux/actions";
import { useHistory } from "react-router-dom";
import { PropTypes } from "prop-types";

const HistoryEntry = props => {
	const history = useHistory();

	const localDate = new Date(props.timestamp);

	const secondsAgo = Math.round((props.now - localDate) / 1000);
	const minutesAgo = Math.round(secondsAgo / 60);
	const hoursAgo = Math.round(minutesAgo / 60);
	const daysAgo = Math.round(hoursAgo / 24);

	let timestamp = "";

	if (hoursAgo >= 24) {
		if (daysAgo === 1) {
			timestamp = "yesterday";
		} else {
			timestamp = daysAgo + " days ago";
		}
	} else if (minutesAgo >= 60) {
		if (hoursAgo === 1) {
			timestamp = hoursAgo + " hour ago";
		} else {
			timestamp = hoursAgo + " hours ago";
		}
	} else if (secondsAgo >= 60) {
		if (minutesAgo === 1) {
			timestamp = minutesAgo + " minute ago";
		} else {
			timestamp = minutesAgo + " minutes ago";
		}
	} else {
		timestamp = "just now";
	}

	function handleSearch(entryValue) {
		const safeSearchValue = entryValue
			.replace(/\\/g, "\\\\")
			.replace(/"/g, '\\"');
		props.dispatch(setSearchValue(safeSearchValue));
		props.dispatch(resetPage());
		history.push("/prosjekt3/");
	}

	return (
		<ListItem onClick={() => handleSearch(props.entry)}>
			<ListItemGraphic icon='history' />
			<span className='history-searchvalue'>{props.entry}</span>
			<ListItemMeta>{timestamp}</ListItemMeta>
		</ListItem>
	);
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(HistoryEntry);

HistoryEntry.propTypes = {
	entry: PropTypes.string,
	timestamp: PropTypes.string,
	now: PropTypes.instanceOf(Date),
};
