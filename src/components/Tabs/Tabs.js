import React, { useState, useEffect } from "react";

import { TabBar, Tab } from "@rmwc/tabs";
import { Link, useLocation } from "react-router-dom";

import "./Tabs.scss";
import { connect } from "react-redux";

const Tabs = props => {
	const [activeTab, setActiveTab] = useState(0);
	const [showHistory, setShowHistory] = useState(false);
	const location = useLocation();

	useEffect(() => {
		if (location.pathname === "/prosjekt3/") {
			if (activeTab !== 0) {
				setActiveTab(0);
			}
		} else if (location.pathname === "/prosjekt3/wordcloud") {
			if (activeTab !== 1) {
				setActiveTab(1);
			}
		} else if (location.pathname === "/prosjekt3/history") {
			if (activeTab !== 2) {
				setActiveTab(2);
			}
		}
	}, [location.pathname, activeTab]);

	useEffect(() => {
		if (!showHistory && props.user.id) {
			setShowHistory(true)
		}
		else if (showHistory && !props.user.id) {
			setShowHistory(false)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [props.user])

	return (
		<div className='tabs'>
			<TabBar
				activeTabIndex={activeTab}
				onActivate={event => setActiveTab(event.detail.index)}
			>
				<Link to='/prosjekt3/'>
					<Tab className='tab' icon='search'>
						Search
					</Tab>
				</Link>
				<Link to='/prosjekt3/wordcloud'>
					<Tab className='tab wordcloud' icon='cloud'>
						Wordcloud
					</Tab>
				</Link>
				{showHistory ? (
					<Link to='/prosjekt3/history'>
						<Tab className='tab tab-history' icon='history'>
							History
						</Tab>
					</Link>
				) : (
					undefined
				)}
			</TabBar>
		</div>
	);
};

const mapStateToProps = state => ({
	user: state.user,
});

export default connect(mapStateToProps)(Tabs);
