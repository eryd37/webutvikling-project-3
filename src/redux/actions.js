import {
	SET_SORT_FIELD,
	TOGGLE_SORT_DESC,
	SET_RECIPE_SEARCH_VALUE,
	SET_RECIPE_LIMIT,
	SET_MAX_INGREDIENTS,
	SET_MAX_MINUTES,
	SET_MAX_STEPS,
	NEXT_RECIPE_PAGE,
	PREV_RECIPE_PAGE,
	SET_LOADING,
	RESET_PAGE,
	SIGN_OUT,
	SET_USER
} from "./actionTypes";

export const setMaxIngredients = maxIngredients => ({
	type: SET_MAX_INGREDIENTS,
	payload: { maxIngredients },
});
export const setMaxSteps = maxSteps => ({
	type: SET_MAX_STEPS,
	payload: { maxSteps },
});
export const setMaxMinutes = maxMinutes => ({
	type: SET_MAX_MINUTES,
	payload: { maxMinutes },
});

export const setSearchValue = searchValue => ({
	type: SET_RECIPE_SEARCH_VALUE,
	payload: { searchValue },
});

export const setRecipeLimit = limit => ({
	type: SET_RECIPE_LIMIT,
	payload: { limit },
});

export const setSortField = sortField => ({
	type: SET_SORT_FIELD,
	payload: { sortField },
});
export const toggleDescList = () => ({
	type: TOGGLE_SORT_DESC,
});

export const nextRecipePage = () => ({
	type: NEXT_RECIPE_PAGE,
});
export const prevRecipePage = () => ({
	type: PREV_RECIPE_PAGE,
});
export const resetPage = () => ({
	type: RESET_PAGE,
});

export const setLoading = loading => ({
	type: SET_LOADING,
	payload: { loading },
});

export const setUser = (id, token, username) => ({
	type: SET_USER,
	payload: { id, token, username }
})
export const signOut = () => ({
	type: SIGN_OUT
})