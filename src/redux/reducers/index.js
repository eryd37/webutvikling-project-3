import { combineReducers } from "redux";
import recipeFilters from "./recipeFilters";
import recipeSearch from "./recipeSearch";
import recipeLimit from "./recipeLimit";
import recipeSort from "./recipeSort";
import recipePage from "./recipePage";
import loading from "./loading";
import user from "./user"

export default combineReducers({
	recipeFilters: recipeFilters,
	recipeSearch: recipeSearch,
	recipeLimit: recipeLimit,
	recipeSort: recipeSort,
	recipePage: recipePage,
	loading: loading,
	user: user
});
