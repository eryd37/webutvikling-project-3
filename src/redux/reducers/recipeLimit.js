import { SET_RECIPE_LIMIT } from "../actionTypes";

const initialState = {
	limit: 50,
};

const recipeLimit = (state = initialState, action) => {
	switch (action.type) {
		case SET_RECIPE_LIMIT: {
			return { limit: action.payload.limit };
		}
		default: {
			return state;
		}
	}
};
export default recipeLimit;
