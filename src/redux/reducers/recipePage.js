import { NEXT_RECIPE_PAGE, PREV_RECIPE_PAGE, RESET_PAGE } from "../actionTypes";

const initialState = {
	page: 0,
};

const recipePage = (state = initialState, action) => {
	switch (action.type) {
		case NEXT_RECIPE_PAGE: {
			return { page: state.page + 1 };
		}
		case PREV_RECIPE_PAGE: {
			return { page: state.page - 1 };
		}
		case RESET_PAGE: {
			return { page: 0 };
		}
		default: {
			return state;
		}
	}
};
export default recipePage;
