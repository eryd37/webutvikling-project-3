import {
	SIGN_OUT, SET_USER
} from "../actionTypes";

const initialState = {
  id: "",
  token: "",
  username: ""
};

const user = (state = initialState, action) => {
	switch (action.type) {
		case SET_USER: {
      return action.payload;
    }
    case SIGN_OUT: {
      return initialState;
    }
		default: {
			return state;
		}
	}
};
export default user;
