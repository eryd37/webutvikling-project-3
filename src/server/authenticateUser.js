const jsonWebToken = require('jsonwebtoken');
const { SECRET_KEY } = require('../config');

// An Express middleware function that authenticates a user from a token
// The token is packed in as a header in the context from the GQL-Express server
module.exports = (context) => {
    // context comes as an object with headers { ... headers}
    const authHeader = context.req.headers.authorization;

    if (authHeader) {
        // Conventional use of tokens is to send the header on the form: Bearer [token]
        const token = authHeader //.split('Bearer ')[1];

        if (token) {
            try {
                const user = jsonWebToken.verify(token, SECRET_KEY);
                return user;
            } catch (error) {
                throw new Error('Bad token')
            }
        }

        throw new Error('Auth token has the wrong format');
    }

    throw new Error('Auth header was not provided');
}
