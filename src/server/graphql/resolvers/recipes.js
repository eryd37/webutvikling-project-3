const Recipe = require("../../models/Recipe");

module.exports = {
	Mutation: {
		createRecipe: async (
			_,
			{ name, minutes, tags, steps, description, ingredients }
		) => {
			return await Recipe.create(
				{
					name,
					minutes,
					tags,
					n_steps: steps.length,
					steps,
					description,
					n_ingredients: ingredients.length,
					ingredients,
				},
				(error, doc) => {
					if (error) {
						console.error(error);
						return error;
					} else {
						return doc;
					}
				}
			);
		},
		deleteRecipe: async (_, { recipeID }) => {
			await Recipe.deleteOne({ _id: recipeID }, error => {
				return error;
			});
			return true;
		},
	},
	Query: {
		recipe: async (_, { recipeID }) => {
			return await Recipe.findById(recipeID);
		},
		closestRecipe: async (
			_,
			{ name, description, minutes, tags, ingredients, steps }
		) => {
			return await Recipe.findOne({
				name: name,
				minutes: minutes,
				ingredients: ingredients,
				steps: steps,
				description: description,
				tags: tags,
			});
		},
		recipes: async (
			_,
			{
				searchValue,
				start,
				limit,
				sortField,
				sortDESC,
				minMinutes,
				maxMinutes,
				minIngredients,
				maxIngredients,
				minSteps,
				maxSteps,
			}
		) => {
			const ensuredSortField = sortField ? sortField : "_id";
			const safeSortField = ensuredSortField
				.replace(/\\/g, "")
				.replace(/"/g, '\\"');
			const ensuredSortDirection = sortDESC ? "-" : "";
			const ensuredMinuteRange = {
				min: minMinutes ? minMinutes : 0,
				max: maxMinutes ? maxMinutes : Infinity,
			};
			const ensuredIngredientsRange = {
				min: minIngredients ? minIngredients : 0,
				max: maxIngredients ? maxIngredients : Infinity,
			};
			const ensuredStepsRange = {
				min: minSteps ? minSteps : 0,
				max: maxSteps ? maxSteps : Infinity,
			};
			let searchQuery = undefined;
			if (searchValue) {
				searchQuery = Recipe.find(
					{
						$text: { $search: searchValue },
						minutes: {
							$gte: ensuredMinuteRange.min,
							$lte: ensuredMinuteRange.max,
						},
						n_ingredients: {
							$gte: ensuredIngredientsRange.min,
							$lte: ensuredIngredientsRange.max,
						},
						n_steps: {
							$gte: ensuredStepsRange.min,
							$lte: ensuredStepsRange.max,
						},
					},
					{ score: { $meta: "textScore" } }
				);
			} else {
				searchQuery = Recipe.find({}, { score: { $meta: "textScore" } });
			}
			const sortedResult = await searchQuery
				.sort(ensuredSortDirection + safeSortField)
				.sort({ score: { $meta: "textScore" } })
				.skip(start ? start : 0)
				.limit(limit ? limit : 50);
			return sortedResult;
		},
		wordcloud: async () => {
			return Recipe.aggregate([
				{
					$project: {
						name: { $split: ["$name", " "] },
					},
				},
				{ $unwind: "$name" },
				{
					$match: {
						$expr: {
							$and: [
								{ $gte: [{ $strLenCP: "$name" }, 3] },
								{ $not: { $in: ["$name", ["the", "and", "with", "for"]] } },
							],
						},
					},
				},
				{
					$group: {
						_id: "$name",
						count: { $sum: 1 },
					},
				},
				{ $project: { _id: 0, word: "$_id", count: 1 } },
				{ $sort: { count: -1 } },
				{ $limit: 50 },
			]);
		},
	},
};
