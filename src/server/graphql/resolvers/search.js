const Search = require("../../models/Search");
const authenticateUser = require("../../authenticateUser");

module.exports = {
	Query: {
        getSearchHistory: async (_, { userID }) => {
            try {
                const searchHistory = await Search.find({ userID: userID }).sort({ createdAt: -1 });
                return searchHistory;
            }   catch (error) {
                throw new Error(error);
            }
        },
        getSearch: async (_, { searchID }) => {
            try{
                const search = await Search.findById(searchID);
                if (search) {
                    return search;
                }   else {
                    throw new Error('Search not found');
                }
            } catch (error) {
				throw new Error(error);
            }
        }
    },
    Mutation: {
        addHistory: async (_, { searchValue }, context) => {
            const user = authenticateUser(context);

			const newSearch = new Search({
                searchValue,
                userID: user._id,
				username: user.username,
				createdAt: new Date().toISOString()
			});

			const search = await newSearch.save();

			return search;
        }
    }
}