const { model, Schema } = require('mongoose');

const searchSchema = new Schema({
    searchValue: String,
    userID: String,
    username: String,
    createdAt: String,

    // Links the "Search" data model to a User
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    }
});

module.exports = model('Search', searchSchema);